
# Vokabellisten

Alle Vokabeln sind auf Kroatisch und Deutsch und in lateinischer Schrift.

*Zur Schreibweise*

Verben stehen im Infinitiv, der Aspekt wird nur angegeben, wenn beide vorkommen. Nomen stehen im Singular, außer das deutsche Nomen steht im Plural. Adjektive stehen, wenn nicht anders angegeben, in der männlichen, unbetonten Form und im Singular. Wird das Adverb gesucht, ist das extra angegeben.

Groß-/Kleinschreibung und das kroatische Alphabet werden beachtet. Großbuchstaben am Satzanfang und etwaige Satzzeichen sind in der kroatischen und deutschen Version konsistent.

*Abkürzungen*

- **sv.** und **nesv.**: Vollendeter/perfektiver bzw unvollendeter/imperfektiver Aspekt, bei Verben. Von kroatisch: (ne)svršeni vid
- **sr.**: Bei manchen Vokabeln steht auch die serbische Version, zB bijela, sr: beo
- **adj.** für Adjektive, **adv.** für Adverben
- **2x**: Zwei kroatische Synonyme, beide sollte man sich merken
- **ti & Vi**: Auf kroatisch steht die normale und die Höflichkeitsform, beide sollte man sich merken
- **Sg.** und **Pl.** für Wörter im Singular oder Plural
- **m.**, **w.** und **s.** für die Geschlechter

*CSV Format*

Es gibt vier Spalten, sie folgen dem Format meiner Vokabel-App: kroatisch, deutsch, Hinweis, Zusatz

Die Einträge sind durch Beistriche getrennt.