# README

Dieses Repo sammelt Resourcen zum Lernen der Sprache BKMS (bosnisch-kroatisch-montenegrinisch-serbisch)

**Name:** `nana-learns-bks` (from my nickname)

**Authors and acknowledgments**

Folgende Vokabellisten wurden aus meinem Sprachkurs kopiert bzw zusammengeschrieben.

- `vok-anleitungen-grammatiken`
- `vok-lektion*.csv`: Lektionen 1 bis 8
- `vok-brojevi.csv`

Kurs *935.099 B/K/S, SL/IT, Grundstufe 1&2* von *treffpunkt-sprachen* an der Universität Graz. Dieser intensive, aber sehr gute Kurs ist für Studierende der Universität Graz vorgesehen. Treffpunkt-sprachen bietet aber noch viele weitere Fremdsprachenkurse [hier](https://treffpunktsprachen.uni-graz.at/de/lehre/fremdsprachenkurse/) an, auch für externe Teilnehmer:innen. Klammer auf unentgeltliche Werbung Ende Klammer zu.

Der Rest kommt, wenn nicht anders angegeben, von mir, Anna Brandstätter.

**Support:**

Bis jetzt gibt es noch zusätzliche Informationen in der Datei `notes.md` für und in folgendenen Ordnern:

- vocab

Für weiteres könnt ihr mich gerne kontaktieren.

**Contributing:**

Wenn euch Fehler oder Abweichungen von den unten angegeben Konventionen auffallen - bitte gerne bei mir melden.

**Content:**

Ich plane, noch weiteren Inhalt zu sammeln. Bis jetzt gibts folgendes:

- Diese Datei `README.md` als Einstiegspunkt
- Ordner vocab: Vokabellisten. Im Laufe des Kurses werde ich die Vokabeln neuer Lektionen ergänzen. Die Vokabellisten werden gegebenfalls laufend korrigiert - für Hinweise gebührt euch meine ewig währende Dankbarkeit!

**Über GitLab**

Projekte wie dieses heißen hier "repositories", oder kurz repos. Ihr könnt entweder das ganze Repo herunterladen, oder einzelne Dateien.
